# spiking

A software development template to write C program and simulate in spike


Install riscv cross compilation tools from [riscv-tools](https://gitlab.com/shaktiproject/software/riscv-tools) for all platforms.

For Ubuntu 16.04, please use ready to use [shakti-tools](https://gitlab.com/shaktiproject/software/shakti-tools.git).

