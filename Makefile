all:
	@riscv64-unknown-elf-gcc -g -Og  -S sample.c  
	@riscv64-unknown-elf-gcc -S sum.c  
	@riscv64-unknown-elf-gcc -S add3.c  
	@riscv64-unknown-elf-gcc -S add1.c  
	@riscv64-unknown-elf-gcc -S add2.c  
	@riscv64-unknown-elf-gcc -g -Og -o deploy.o -c sample.c 
	@riscv64-unknown-elf-gcc -g  -o sum.o -c sum.c 
	@riscv64-unknown-elf-gcc -g -Og -T spike.lds -nostartfiles -o sample.elf deploy.o
	@riscv64-unknown-elf-gcc -g  -T spike.lds -nostartfiles -o sum.elf sum.o
	@riscv64-unknown-elf-gcc -nostdlib -nostartfiles -T spike.lds  example1.S -o example1.elf
	@riscv64-unknown-elf-gcc -nostdlib -nostartfiles -T spike.lds  example2.S -o example2.elf
	@riscv64-unknown-elf-gcc -nostdlib -nostartfiles -T spike.lds  example3.S -o example3.elf
	@riscv64-unknown-elf-gcc -g hello.c -o hello.elf  
	
clean:
	rm  *.elf *.o *.s
